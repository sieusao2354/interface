import './App.css';
import State from './State/State';

function App() {
  return (
    <div className="App">
      <State />

    </div>
  );
}

export default App;
