import React from 'react'
import { useState } from 'react';

export default function AddToCard({ props }) {

    const [nameInput, setNameInput] = useState('');
    const [addressInput, setAddressInput] = useState('');
    const handleAdd = () => {
        if (nameInput && addressInput) {
            const regex = /^[a-zA-Z0-9]*$/;
            if (!regex.test(nameInput) || !regex.test(addressInput)) {
                return alert("Vui lòng chỉ nhập chữ và số, không có ký tự đặc biệt và không có dấu!");
            }
            const confirmed = window.confirm("Are you sure you want to add?");
            if (confirmed) {
                const newCourse = {
                    name: nameInput,
                    address: addressInput,
                };
                props(newCourse);
                setNameInput('');
                setAddressInput('');
            }
        } else {
            alert("Hãy nhập đầy đủ thông tin trước khi thêm mới");
        }
    }
    return (
        <div>
            <form onSubmit={(e) => { e.preventDefault() }}>
                <input
                    className='input-name-add'
                    type="text"
                    placeholder='Name'
                    value={nameInput}
                    onChange={(e) => {
                        setNameInput(e.target.value)
                    }} />
                <input
                    className='input-address-add'
                    type="text"
                    placeholder='Addres'
                    value={addressInput}
                    onChange={(e) => {
                        setAddressInput(e.target.value)
                    }}
                />
                <button onClick={handleAdd} >Add</button>

            </form>

        </div>
    )
}
