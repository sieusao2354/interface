import React, { useState, } from 'react';
import './State.css';
import AddToCard from './AddToCard';
import { useEffect } from 'react';


export default function State() {
    const [selectedId, setSelectedId] = useState(null);
    const [nameError, setNameError] = useState(false);
    const [addressError, setAddressError] = useState(false);

    const [nameInput, setNameInput] = useState('');
    const [addressInput, setAddressInput] = useState('');
    const [course, setCourse] = useState([
        {
            id: 1,
            name: 'Trang',
            address: ' Biên Hòa',
        },
        {
            id: 2,
            name: 'Tân',
            address: ' Biên Hòa',
        },
        {
            id: 3,
            name: 'Nữ',
            address: ' Sài Gòn',
        },
        {
            id: 4,
            name: 'Toàn',
            address: ' Sài Gòn',
        },
    ]);

    const handleAddCourse = (newCourse) => {
        setCourse([...course, { ...newCourse, id: course.length + 1 }])
    }

    const handleSave = (id) => {
        if (!nameInput) {
            setNameError(true);
            return;
        }

        if (!addressInput) {
            setAddressError(true);
            return;
        }

        const updatedCourse = course.map((item) => {
            if (item.id === selectedId) {
                return { ...item, name: nameInput, address: addressInput };
            }
            return item;
        });

        const confirmed = window.confirm("Are you sure you want to Save?");
        if (confirmed) {
            const filteredCourse = course.filter((item) => item.id !== id);
            setCourse(filteredCourse);
        }

        setCourse(updatedCourse);
    };

    const handleCancel = () => {
        setSelectedId(null);
    };


    useEffect(() => {
        const savedCourse = localStorage.getItem('course');
        if (savedCourse) {
            setCourse(JSON.parse(savedCourse));
        }
    }, []);

    useEffect(() => {
        localStorage.setItem('course', JSON.stringify(course));
    }, [course]);

    const handleDelete = (id) => {


        if (selectedId === id) {
            setSelectedId(null); // nếu selectedId đang bằng id của mục sẽ set về null
        } else if (selectedId === null) {
            alert("bạn phải chọn vào để xóa ");
            return;
        }
        const filteredCourse = course.filter((item) => item.id !== id);
        setCourse(filteredCourse);
        setSelectedId(null);
        setNameInput('');
        setAddressInput('');
        const confirmed = window.confirm("Are you sure you want to delete?");
        if (confirmed) {
            const filteredCourse = course.filter((item) => item.id !== id);
            setCourse(filteredCourse);
        }
    };

    const handleClick = (id) => {
        setSelectedId(id === selectedId ? null : id)
        const selectedCourse = course.find((item) => item.id === id);
        setNameInput(selectedCourse.name);
        setAddressInput(selectedCourse.address);
    }

    const handleNameChange = (event) => {
        setNameInput(event.target.value);
    };

    const handleAddressChange = (event) => {
        setAddressInput(event.target.value);
    };






    return (
        <div>
            <AddToCard props={handleAddCourse} />

            <div>

                <ul className="course-list text-white">
                    {course.map((item) => (
                        <li
                            key={item.id}
                            className={`${selectedId === item.id ? 'bg-red-500' : ''} bg-gray-300 text-black`}
                            onClick={() => handleClick(item.id)}
                        >
                            {item.name}
                            <div>
                                {item.address}
                            </div>
                            <div className='bg-black text-white text-center' >
                                <button className='btnDel' onClick={() => handleDelete(item.id)}>Delete</button>
                            </div>
                        </li>


                    ))}
                </ul>
                <div className="course-details">
                    {selectedId && (
                        <div>
                            <div>
                                <div>
                                    <label htmlFor="nameInput">Name: </label>
                                </div>
                                <input
                                    className='nameChangeInput'
                                    type="text"
                                    id="nameInput"
                                    value={nameInput}
                                    onChange={handleNameChange}
                                />
                            </div>
                            <br />
                            <div className='' >
                                <div>
                                    <label className='' htmlFor="x"> Address: </label>

                                </div>

                                <input
                                    className='addressChangeInput'
                                    type="text"
                                    id="addressInput"
                                    value={addressInput}
                                    onChange={handleAddressChange}
                                />
                            </div>

                            <div className='py-5' >
                                <button className='px-5' onClick={handleSave} >Save</button>
                                <button className='px-10' onClick={handleCancel} >Cancel </button>
                            </div>



                        </div>

                    )}
                </div>
            </div >
        </div >
    );
}
